# source: nltk official documentation
# pip install nltk

import nltk
from nltk.corpus import wordnet

nltk.download('wordnet')
synonyms = []
antonyms = []

for syn in wordnet.synsets("active"):
    for l in syn.lemmas():
        synonyms.append(l.name())
        if l.antonyms():
            antonyms.append(l.antonyms()[0].name())

print("Synonyms", set(synonyms))
print("Antonyms", set(antonyms))
